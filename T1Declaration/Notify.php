<?php
/**
 * Created by IntelliJ IDEA.
 * User: VMikhaylov
 * Date: 05.09.2014
 * Time: 12:54
 */

set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme');
//set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/ncts');
//set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme/fi/tu');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';

class CustomsNotifyCallback {

    function Notify($args) {

        /** @var $notifyRequestHeader NotifyRequestHeader */
        $notifyRequestHeader = $args[0];

        /** @var $messageInformation MessageInformation */
        $messageInformation = $args[1];

        $notifyResponseHeader = new NotifyResponseHeader($notifyRequestHeader->IntermediaryBusinessId, date('c'));

        $result = new NotifyResponse();
        $result->ResponseHeader = $notifyResponseHeader;
        $result->ResponseCode = '000';
        $result->ResponseText = 'OK';

        return $result;
    }
}

ini_set("soap.wsdl_cache_enabled", "0"); // отключаем кэширование WSDL

$replaceTypes = array('RequestHeader' => "NotifyRequestHeader");

$server = new \com\mikebevz\xsd2php\SoapServer('tulli\dme\NotificationService.wsdl', null, $replaceTypes);
$server->setClass("CustomsNotifyCallback");
$server->handle();


?>