<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '15FI000000002204T9',

    'FunctionCode' => '6',
    'TransitTypeCode' => 'T1',

    'TransitPresentationOffice' => array(
        'CustomsOfficeCode' => 'FI534200'
    ),

    'UnloadingRemarks' => array(
        'RemarksDate' => '2015-01-13',
        'LocationName' => 'FI534200'
    ),

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 30
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
//        'Name' => 'Pochta.fi Oy',             // not allowed for ActualAuthorisedConsignee
//        'Address' => array(
//            'Line' => 'Kultakuusenkuja 4',
//            'PostcodeID' => '55610',
//            'CityName' => 'Imatra',
//            'CountryCode' => 'FI'
//        ),
    ),

    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Artem Lada',
                'Address' => array(
                    'Line' => '420 S 38th St PT 4',
                    'PostcodeID' => '68131',
                    'CityName' => 'OMAHA',
                    'CountryCode' => 'US'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Vacuum Centrifuge',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 30
            ),
//            'NetWeightMeasure' => array(
//                'UnitCode' => 'KGM',
//                'Value' => '29.123'
//            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'CW803748995US',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
//            'PreviousDocument' => array(
//                1 => array(
//                    'DocumentTypeCode' => '740',
//                    'DocumentID' => 'TR-2527'
//                ),
//            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '740',
                    'DocumentID' => 'CW803748995US'
                ),
            ),
            'AdditionalInformation' => null,
//            'TransportEquipment' => array(
//                1 => array('TransportEquipmentID' => 'ABCD1234-5'),
//            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>