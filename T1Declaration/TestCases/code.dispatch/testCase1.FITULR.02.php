<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '14FI000000000438T4',
	
	'FunctionCode' => '6',
	'TransitTypeCode' => 'T1',

    'TransitPresentationOffice' => array(
        'CustomsOfficeCode' => 'FI534200'
    ),
	
	'UnloadingRemarks' => array(
		'RemarksDate' => '2014-11-14',
		'LocationName' => 'FI534200'
	),		

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 1300
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
//        'Name' => 'Pochta.fi Oy',             // not allowed for ActualAuthorisedConsignee
//        'Address' => array(
//            'Line' => 'Kultakuusenkuja 4',
//            'PostcodeID' => '55610',
//            'CityName' => 'Imatra',
//            'CountryCode' => 'FI'
//        ),
    ),

    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'EE',
            'DestinationCountryCode' => 'PL',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS AUTOVEOD-TEHNIKA',
                'Address' => array(
                    'Line' => 'AARDLA 23',
                    'PostcodeID' => 'EE50110',
                    'CityName' => 'TARTU',
                    'CountryCode' => 'EE'
                ),
            ),
//            'ConsignorSecurity' => array(
//                'ID' => null,
//                'IDExtension' => null,
//                'Name' => 'AS AUTOVEOD-TEHNIKA',
//                'Address' => array(
//                    'Line' => 'AARDLA 23',
//                    'PostcodeID' => 'EE50110',
//                    'CityName' => 'TARTU',
//                    'CountryCode' => 'EE'
//                ),
//            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AWISTA LTD. CO.',
                'Address' => array(
                    'Line' => '3-123456789 PRZYTOROWA STREET 98765',
                    'PostcodeID' => '16-400',
                    'CityName' => 'SUWALKI',
                    'CountryCode' => 'PL'
                ),
            ),
//            'ConsigneeSecurity' => array(
//                'ID' => null,
//                'IDExtension' => null,
//                'Name' => 'AWISTA LTD. CO.',
//                'Address' => array(
//                    'Line' => '3-123456789 PRZYTOROWA STREET 98765',
//                    'PostcodeID' => '16-400',
//                    'CityName' => 'SUWALKI',
//                    'CountryCode' => 'PL'
//                ),
//            ),
            'Commodity' => array(
                'TariffClassification' => array(940510),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'KATTOVALAISIMIA',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 30
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '29.123'
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'MALLI JOKAKODIN TUNNELMAVALO TALVI/2007',
                    'PackageQuantity' => 15678,
                    'PieceCountQuantity' => null
                )
            ),
//            'PreviousDocument' => array(
//                1 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2527'
//                ),
//                2 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2528'
//                ),
//                3 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2529'
//                ),
//                4 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2530'
//                ),
//                5 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2531'
//                ),
//                6 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2532'
//                ),
//                7 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2533'
//                ),
//                8 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2534'
//                ),
//                9 => array(
//                    'DocumentTypeCode' => '71',
//                    'DocumentID' => 'TR-2535'
//                )
//            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'ABCD1234-5'),
            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>