<?php

$params = array(

    'XMessageType' => 'FITARR',

    'TraderReferenceID' => 'Tehtävä4-FITARR',

    'MovementReferenceID' => '14FI000000000442T4',
	
    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),
	'NotificationDate' => '2014-11-14',
	'SealConditionIndicator' => 1,      // 'EI OK',
	'DestinationLanguageCode' => 'FI',

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Kultakuusenkuja 4',
            'PostcodeID' => '55610',
            'CityName' => 'Imatra',
            'CountryCode' => 'FI'
        ),
    ),

    'EnRouteEvent' => array(
        'LocationName' => 'Porvoo',
        'CountryCode' => 'FI',
        'Incident' => array(
            'IncidentIndicator' => 1,
            'Description' => 'Rengasrikon vuoksi auto ajoi ojaan. Tavarat lastattiin onnettomuuspaikalla toiseen perävaunuun. Kuorma jatkoi sinetöimättömänä matkaa.',
            'Endorsement' => array(
                'EndorsementDate' => '2004-03-03',
                'AuthorityDescription' => 'Porvoon poliisi',
//                'AuthorityLocationDescription' => '',
                'AuthorityCountryCode' => 'FI'
            )
        ),
        'TransShipment'	=> array(
            'Endorsement' => array(
                'EndorsementDate' => '2004-03-03',
                'AuthorityDescription' => 'Porvoon poliisi',
                'AuthorityLocationDescription' => 'Porvoo',
                'AuthorityCountryCode' => 'FI'
            ),
            'NewTransportMeans' => array(
                'TransportMeansID' => 'HVU-116/PYP-754',
                'TransportMeansNationalityCode' => 'FI'
            )
        )
    )

/*
	
	EnRouteEvent
		LocationName = Porvoo
		CountryCode = FI
		
		Incident
			IncidentIndicator = 1
			Description = Rengasrikon vuoksi auto ajoi ojaan. Tavarat lastattiin onnettomuuspaikalla toiseen perävaunuun. Kuorma jatkoi sinetöimättömänä matkaa.			
				
			Endorsement
				EndorsementDate	= 20040303
				AuthorityDescription = Porvoon poliisi
				AuthorityCountryCode = FI
		Control
			NotifiedIndicator
		TransShipment			
			Endorsement
				EndorsementDate	= 20040303
				AuthorityDescription = Porvoon poliisi
				AuthorityLocationDescription = Porvoo								
				AuthorityCountryCode = FI
			NewTransportMeans
				TranportMeansID = HVU-116/PYP-754
				TranportMeansNationalityCode = FI
*/
	
);

?>