<?php

$params = array(

    'XMessageType' => 'FITARR',

    'TraderReferenceID' => 'Tehtävä5-FITARR',

    'MovementReferenceID' => '14FI000000000532T7',
	
    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),
	'NotificationDate' => date('Y-m-d'),
	'SealConditionIndicator' => 1,      // 'OK',
	'DestinationLanguageCode' => 'SV',

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Kultakuusenkuja 4',
            'PostcodeID' => '55610',
            'CityName' => 'Imatra',
            'CountryCode' => 'FI'
        ),
    )
);

?>