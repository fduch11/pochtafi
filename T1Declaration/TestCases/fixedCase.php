<?php

$fixedParams = array(

    'BusinessId' => 'FI2628792-7',
    'SoftwareInfo' => 'QA Tester',
    'Environment' => 'TEST',
    'RecipientId' => 'FI0245442-8',

    'Principal' => array(
        'ID' => '2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Kultakuusenkuja 4',
            'PostcodeID' => '55610',
            'CityName' => 'Imatra',
            'CountryCode' => 'FI'
        ),
        'NatureQualifierCode' => 'R',
        'LanguageCode' => 'FI',
        'TIRHolderID' => null
    ),

    'RepresentativePerson' => 'Galina Tynni 0505902919',

    'Guarantee' => array(
        'TransitGuaranteeTypeCode' => '1',
        'GuaranteeReferenceID' => '14FI002006P000045',
        'OtherGuaranteeReferenceID' => null,
        'AccessKeyCodeID' => 'c5oy',
        'ValidityLimitationIndicator' => '0',
        'ValidityLimitationCountryCode' => null
    ),

    'TransitDepartureOffice' => 'FI534200',
);

?>