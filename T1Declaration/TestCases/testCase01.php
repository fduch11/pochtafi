﻿<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä1-ABCD-12345678',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'DKK-275 PBB-123',
        'ConveyanceReferenceID' => null
    ),
//            'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'PL351010',

    'GoodsItemQuantity' => '1',
    'TotalPackageQuantity' => 15678,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 30
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+1 days 06:30")), // +1 день 06:30 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+4 days")), // +4 дней
    'ContainerTransportIndicator' => 'true',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'HE046-E'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'EE',
            'DestinationCountryCode' => 'PL',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS AUTOVEOD-TEHNIKA',
                'Address' => array(
                    'Line' => 'AARDLA 23',
                    'PostcodeID' => 'EE50110',
                    'CityName' => 'TARTU',
                    'CountryCode' => 'EE'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS AUTOVEOD-TEHNIKA',
                'Address' => array(
                    'Line' => 'AARDLA 23',
                    'PostcodeID' => 'EE50110',
                    'CityName' => 'TARTU',
                    'CountryCode' => 'EE'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AWISTA LTD. CO.',
                'Address' => array(
                    'Line' => '3-123456789 PRZYTOROWA STREET 98765',
                    'PostcodeID' => '16-400',
                    'CityName' => 'SUWALKI',
                    'CountryCode' => 'PL'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AWISTA LTD. CO.',
                'Address' => array(
                    'Line' => '3-123456789 PRZYTOROWA STREET 98765',
                    'PostcodeID' => '16-400',
                    'CityName' => 'SUWALKI',
                    'CountryCode' => 'PL'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(940510),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'KATTOVALAISIMIA',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 30
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '29.123'
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'MALLI JOKAKODIN TUNNELMAVALO TALVI/2007',
                    'PackageQuantity' => 15678,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2527'
                ),
                2 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2528'
                ),
                3 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2529'
                ),
                4 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2530'
                ),
                5 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2531'
                ),
                6 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2532'
                ),
                7 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2533'
                ),
                8 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2534'
                ),
                9 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'TR-2535'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'ABCD1234-5'),
            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>