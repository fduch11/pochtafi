<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä9-lisäsevitys',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'DKK-275 PBB-150',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'PL351010',

    'GoodsItemQuantity' => '1',
    'TotalPackageQuantity' => 15678,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 30
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+1 days 06:50")), // +1 день 6:50 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+4 days")), // +4 дня
    'ContainerTransportIndicator' => 'true',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'HE0465-E'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'EE',
            'DestinationCountryCode' => 'PL',
            'Consignor' => array(
                'ID' => 'FI0123456-7',
                'IDExtension' => 'T0001',
                'Name' => 'Eest Tehnika As',
                'Address' => array(
                    'Line' => 'Tartu Mnt. 68',
                    'PostcodeID' => '10144',
                    'CityName' => 'Tallinn',
                    'CountryCode' => 'EE'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => 'FI0123456-7',
                'IDExtension' => 'T0001',
                'Name' => 'Eest Tehnika As',
                'Address' => array(
                    'Line' => 'Tartu Mnt. 68',
                    'PostcodeID' => '10144',
                    'CityName' => 'Tallinn',
                    'CountryCode' => 'EE'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Awista Ltd. Co.',
                'Address' => array(
                    'Line' => '3-123 Przytorowa Street',
                    'PostcodeID' => '16-400',
                    'CityName' => 'Suwalki',
                    'CountryCode' => 'PL'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Awista Ltd. Co.',
                'Address' => array(
                    'Line' => '3-123 Przytorowa Street',
                    'PostcodeID' => '16-400',
                    'CityName' => 'Suwalki',
                    'CountryCode' => 'PL'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(262019),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'kattovalaisimia',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 30
            ),
            'NetWeightMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '29.123'
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'Lilled: 1-5 ÖX+ÄD-punased-sinised',
                    'PackageQuantity' => 15678,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 71,
                    'DocumentID' => 'TR-2527 22.1.2007'
                )
            ),

            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => array(
                1 => array('TransportEquipmentID' => 'ABCD12345-6'),
                2 => array('TransportEquipmentID' => 'ABCD12345-7')
            ),
            'FreightPaymentMethodCode' => null

        )
    )
);

?>