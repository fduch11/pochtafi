<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä10-virhesanoma',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'FOR-456  PBB-345',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'IT015101',

    'GoodsItemQuantity' => '1',
    'TotalPackageQuantity' => 125,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 2500
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("today 11:15")), // текущая 11:15 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+5 days")), // +5 дней
    'ContainerTransportIndicator' => 'false',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'VL036-0006459'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'RU',
            'DestinationCountryCode' => 'IT',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Superior Household Ltd.',
                'Address' => array(
                    'Line' => 'Nevsky Prospect 108',
                    'PostcodeID' => '193036',
                    'CityName' => 'St. Petersburg ',
                    'CountryCode' => 'RU'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Superior Household Ltd.',
                'Address' => array(
                    'Line' => 'Nevsky Prospect 108',
                    'PostcodeID' => '193036',
                    'CityName' => 'St. Petersburg ',
                    'CountryCode' => 'RU'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Model Milano Ltd.',
                'Address' => array(
                    'Line' => 'Piazza Lima 2',
                    'PostcodeID' => '20124',
                    'CityName' => 'Milano',
                    'CountryCode' => 'IT'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Model Milano Ltd.',
                'Address' => array(
                    'Line' => 'Piazza Lima 2',
                    'PostcodeID' => '20124',
                    'CityName' => 'Milano',
                    'CountryCode' => 'IT'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(293941),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'tekstiiliä',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2500
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'Rose Garden',
                    'PackageQuantity' => 125,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 71,
                    'DocumentID' => '712 2.1.2007'
                )
            ),

            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null

        )
    )
);

?>