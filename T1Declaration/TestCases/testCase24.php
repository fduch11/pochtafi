﻿<?php

$params = array(

    'TraderReferenceID' => 'Shipping 4',

    'TransitControlResultCode' => 'A3',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'RU',
        'TransportMeansID' => 'B777BB 78',
        'ConveyanceReferenceID' => null
    ),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI556100',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => '1'
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s'),
        'LocationName' => 'FI556100'
    ),

    'Unloading' => array(
        'LocationName' => 'St.-Petersburg'
    ),
	
    'Issue' => array(
        'IssueDate' => date('Y-m-d'),
        'LocationName' => 'Lappeenranta'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+3 day")),
    'ContainerTransportIndicator' => 'false',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'DJPremium.com',
                'Address' => array(
                    'Line' => '7720 Kenamar Court Suite C',
                    'PostcodeID' => 'CA 92121',
                    'CityName' => 'San Diego',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'DJPremium.com',
                'Address' => array(
                    'Line' => '7720 Kenamar Court Suite C',
                    'PostcodeID' => 'CA 92121',
                    'CityName' => 'San Diego',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Juri Karpenko',
                'Address' => array(
                    'Line' => 'Nevsky pr. 5',
                    'PostcodeID' => '190000',
                    'CityName' => 'St.-Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Juri Karpenko',
                'Address' => array(
                    'Line' => 'Nevsky pr. 5',
                    'PostcodeID' => '190000',
                    'CityName' => 'St.-Petersburg',
                    'CountryCode' => 'RU'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(640399),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Shoes',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '1'
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '1703851',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 'VV',
                    'DocumentID' => '1234568'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>