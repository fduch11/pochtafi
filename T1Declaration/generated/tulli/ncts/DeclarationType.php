<?php

/**
 * @xmlNamespace 
 * @xmlType TransportMeansType
 * @xmlName DeclarationType
 * @var DeclarationType
 */
class DeclarationType
{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType $MovementReferenceID [optional] Movement reference number (MRN), issued by Customs.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\UniqueConsignmentReferenceIDType $UniqueConsignmentReferenceID [optional] Unique Consignment Reference (as definec by WCO and ISO 15459) is a unique identifier assigned to goods being subject to cross border transactions.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\TraderReferenceIDType $TraderReferenceID [optional] Trader asssigned reference for a document.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType $TransitTypeCode [optional] Transit declaration type code, FI Customs code list 0081.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\FunctionCodeType $FunctionCode [optional] Mesage function code, FI Customs code list 0093.
		@param  $Issue [optional] Issuing details.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DispatchCountryCode [optional] Country of dispatch.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DestinationCountryCode [optional] Destination country.
		@param  $Itinerary [optional] The chronological order the routing countries, through which the goods pass through between the original country of dispatch and the final destination country.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $BindingItineraryExemptionIndicator [optional] Indicates an exemption from the binding itinerary.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $SecurityInformationIndicator [optional] Indicates where the NCTS is used or not for safety and security purposes.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\TransitControlResultCodeType $TransitControlResultCode [optional] Transit control result code.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\DateType $TransitLimitDate [optional] The date when transit is presented at the destination customs office.
		@param fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType $TransitDepartureOffice [optional] Customs office where transit procedure starts.
		@param fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType $TransitDestinationOffice [optional] Customs office where goods are to be presented for release from transit procedure.
		@param fi\tulli\schema\external\ncts\dme\v1\TransitBorderOfficeContactType $TransitBorderOffice [optional] Intended  arrival  customs  office  in  each  EFTA  country,  which  the  goods  pass  though  and  the  arrival  customs  office, through which the goods are re-imported to the Community customs area after passing through an EFTA country, or if transit passes through a country which is not a member in the European Community of EFTA (= customs transit area), the exit customs office through which the goods leave the transit area and the arrival customs office through which the goods once again arrive to the transit area.
		@param  $Loading [optional] Loading information.
		@param  $Unloading [optional] Unloading information.
		@param fi\tulli\schema\external\ncts\dme\v1\GoodsLocationType $GoodsLocation [optional] Goods location information.
		@param  $DepartureTransportMeans [optional] Information on the means of transport on which the goods are directly loaded at the time of transit formalities.
		@param  $BorderTransportMeans [optional] Information on the means of transport crossing the Community border as it is known at the time of completing transit formalities.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $ContainerTransportIndicator [optional] Indicates, wether the goods are or are presumed to be transported in container(s) when crossing the Community border.
		@param fi\tulli\schema\external\ncts\dme\v1\SealingType $Sealing [optional] Seals information.
		@param fi\tulli\schema\external\ncts\dme\v1\GoodsItemQuantityType $GoodsItemQuantity [optional] Goods item quantity.
		@param fi\tulli\schema\external\ncts\dme\v1\TotalPackageQuantityType $TotalPackageQuantity [optional] Total package quantity.
		@param fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType $TotalGrossMassMeasure [optional] Total gross mass for all goods items in transit declaration.
		@param fi\tulli\schema\external\ncts\dme\v1\PartyType $Consignor [optional] Consignor party.
		@param fi\tulli\schema\external\ncts\dme\v1\PartyType $ConsignorSecurity [optional] Consignor security party.
		@param fi\tulli\schema\external\ncts\dme\v1\PartyType $Consignee [optional] Consignee party.
		@param fi\tulli\schema\external\ncts\dme\v1\PartyType $ConsigneeSecurity [optional] Consignee security party.
		@param  $AuthorisedConsignee [optional] Authorised consignee party.
		@param fi\tulli\schema\external\ncts\dme\v1\PartyType $Carrier [optional] Carrier party.
		@param fi\tulli\schema\external\ncts\dme\v1\RepresentativePersonPartyType $RepresentativePerson [optional] Person lodging the declaration.
		@param  $Principal [optional] Principal trader party.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\SpecificCircumstanceCodeType $SpecificCircumstanceCode [optional] Specific circumstance code, FI Customs code list 0402.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\FreightPaymentMethodCodeType $FreightPaymentMethodCode [optional] Transport charges payment method code, FI Customs code list 0327.
		@param  $Guarantee [optional] Guarantee details.
	*/                                                                        
	public function __construct($MovementReferenceID = null, $UniqueConsignmentReferenceID = null, $TraderReferenceID = null, $TransitTypeCode = null, $FunctionCode = null, $Issue = null, $DispatchCountryCode = null, $DestinationCountryCode = null, $Itinerary = null, $BindingItineraryExemptionIndicator = null, $SecurityInformationIndicator = null, $TransitControlResultCode = null, $TransitLimitDate = null, $TransitDepartureOffice = null, $TransitDestinationOffice = null, $TransitBorderOffice = null, $Loading = null, $Unloading = null, $GoodsLocation = null, $DepartureTransportMeans = null, $BorderTransportMeans = null, $ContainerTransportIndicator = null, $Sealing = null, $GoodsItemQuantity = null, $TotalPackageQuantity = null, $TotalGrossMassMeasure = null, $Consignor = null, $ConsignorSecurity = null, $Consignee = null, $ConsigneeSecurity = null, $AuthorisedConsignee = null, $Carrier = null, $RepresentativePerson = null, $Principal = null, $SpecificCircumstanceCode = null, $FreightPaymentMethodCode = null, $Guarantee = null)
	{
		$this->MovementReferenceID = $MovementReferenceID;
		$this->UniqueConsignmentReferenceID = $UniqueConsignmentReferenceID;
		$this->TraderReferenceID = $TraderReferenceID;
		$this->TransitTypeCode = $TransitTypeCode;
		$this->FunctionCode = $FunctionCode;
		$this->Issue = $Issue;
		$this->DispatchCountryCode = $DispatchCountryCode;
		$this->DestinationCountryCode = $DestinationCountryCode;
		$this->Itinerary = $Itinerary;
		$this->BindingItineraryExemptionIndicator = $BindingItineraryExemptionIndicator;
		$this->SecurityInformationIndicator = $SecurityInformationIndicator;
		$this->TransitControlResultCode = $TransitControlResultCode;
		$this->TransitLimitDate = $TransitLimitDate;
		$this->TransitDepartureOffice = $TransitDepartureOffice;
		$this->TransitDestinationOffice = $TransitDestinationOffice;
		$this->TransitBorderOffice = $TransitBorderOffice;
		$this->Loading = $Loading;
		$this->Unloading = $Unloading;
		$this->GoodsLocation = $GoodsLocation;
		$this->DepartureTransportMeans = $DepartureTransportMeans;
		$this->BorderTransportMeans = $BorderTransportMeans;
		$this->ContainerTransportIndicator = $ContainerTransportIndicator;
		$this->Sealing = $Sealing;
		$this->GoodsItemQuantity = $GoodsItemQuantity;
		$this->TotalPackageQuantity = $TotalPackageQuantity;
		$this->TotalGrossMassMeasure = $TotalGrossMassMeasure;
		$this->Consignor = $Consignor;
		$this->ConsignorSecurity = $ConsignorSecurity;
		$this->Consignee = $Consignee;
		$this->ConsigneeSecurity = $ConsigneeSecurity;
		$this->AuthorisedConsignee = $AuthorisedConsignee;
		$this->Carrier = $Carrier;
		$this->RepresentativePerson = $RepresentativePerson;
		$this->Principal = $Principal;
		$this->SpecificCircumstanceCode = $SpecificCircumstanceCode;
		$this->FreightPaymentMethodCode = $FreightPaymentMethodCode;
		$this->Guarantee = $Guarantee;
	}
	
	/**
	 * @Definition Movement reference number (MRN), issued by Customs.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName MovementReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType
	 */
	public $MovementReferenceID;
	/**
	 * @Definition Unique Consignment Reference (as definec by WCO and ISO 15459) is a unique identifier assigned to goods being subject to cross border transactions.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName UniqueConsignmentReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\UniqueConsignmentReferenceIDType
	 */
	public $UniqueConsignmentReferenceID;
	/**
	 * @Definition Trader asssigned reference for a document.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TraderReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\TraderReferenceIDType
	 */
	public $TraderReferenceID;
	/**
	 * @Definition Transit declaration type code, FI Customs code list 0081.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType
	 */
	public $TransitTypeCode;
	/**
	 * @Definition Mesage function code, FI Customs code list 0093.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName FunctionCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\FunctionCodeType
	 */
	public $FunctionCode;
	/**
	 * @Definition Issuing details.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Issue
	 */
	public $Issue;
	/**
	 * @Definition Country of dispatch.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DispatchCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DispatchCountryCode;
	/**
	 * @Definition Destination country.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DestinationCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DestinationCountryCode;
	/**
	 * @Definition The chronological order the routing countries, through which the goods pass through between the original country of dispatch and the final destination country.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Itinerary
	 */
	public $Itinerary;
	/**
	 * @Definition Indicates an exemption from the binding itinerary.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName BindingItineraryExemptionIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $BindingItineraryExemptionIndicator;
	/**
	 * @Definition Indicates where the NCTS is used or not for safety and security purposes.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SecurityInformationIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $SecurityInformationIndicator;
	/**
	 * @Definition Transit control result code.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransitControlResultCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransitControlResultCodeType
	 */
	public $TransitControlResultCode;
	/**
	 * @Definition The date when transit is presented at the destination customs office.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransitLimitDate
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $TransitLimitDate;
	/**
	 * @Definition Customs office where transit procedure starts.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitDepartureOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType
	 */
	public $TransitDepartureOffice;
	/**
	 * @Definition Customs office where goods are to be presented for release from transit procedure.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitDestinationOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType
	 */
	public $TransitDestinationOffice;
	/**
	 * @Definition Intended  arrival  customs  office  in  each  EFTA  country,  which  the  goods  pass  though  and  the  arrival  customs  office, through which the goods are re-imported to the Community customs area after passing through an EFTA country, or if transit passes through a country which is not a member in the European Community of EFTA (= customs transit area), the exit customs office through which the goods leave the transit area and the arrival customs office through which the goods once again arrive to the transit area.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 9
	 * @xmlName TransitBorderOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\TransitBorderOfficeContactType
	 */
	public $TransitBorderOffice;
	/**
	 * @Definition Loading information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Loading
	 */
	public $Loading;
	/**
	 * @Definition Unloading information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Unloading
	 */
	public $Unloading;
	/**
	 * @Definition Goods location information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GoodsLocation
	 * @var fi\tulli\schema\external\ncts\dme\v1\GoodsLocationType
	 */
	public $GoodsLocation;
	/**
	 * @Definition Information on the means of transport on which the goods are directly loaded at the time of transit formalities.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName DepartureTransportMeans
	 */
	public $DepartureTransportMeans;
	/**
	 * @Definition Information on the means of transport crossing the Community border as it is known at the time of completing transit formalities.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName BorderTransportMeans
	 */
	public $BorderTransportMeans;
	/**
	 * @Definition Indicates, wether the goods are or are presumed to be transported in container(s) when crossing the Community border.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ContainerTransportIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $ContainerTransportIndicator;
	/**
	 * @Definition Seals information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Sealing
	 * @var fi\tulli\schema\external\ncts\dme\v1\SealingType
	 */
	public $Sealing;
	/**
	 * @Definition Goods item quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GoodsItemQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\GoodsItemQuantityType
	 */
	public $GoodsItemQuantity;
	/**
	 * @Definition Total package quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TotalPackageQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\TotalPackageQuantityType
	 */
	public $TotalPackageQuantity;
	/**
	 * @Definition Total gross mass for all goods items in transit declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TotalGrossMassMeasure
	 * @var fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType
	 */
	public $TotalGrossMassMeasure;
	/**
	 * @Definition Consignor party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignor
	 * @var fi\tulli\schema\external\ncts\dme\v1\PartyType
	 */
	public $Consignor;
	/**
	 * @Definition Consignor security party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ConsignorSecurity
	 * @var fi\tulli\schema\external\ncts\dme\v1\PartyType
	 */
	public $ConsignorSecurity;
	/**
	 * @Definition Consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignee
	 * @var fi\tulli\schema\external\ncts\dme\v1\PartyType
	 */
	public $Consignee;
	/**
	 * @Definition Consignee security party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ConsigneeSecurity
	 * @var fi\tulli\schema\external\ncts\dme\v1\PartyType
	 */
	public $ConsigneeSecurity;
	/**
	 * @Definition Authorised consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName AuthorisedConsignee
	 */
	public $AuthorisedConsignee;
	/**
	 * @Definition Carrier party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Carrier
	 * @var fi\tulli\schema\external\ncts\dme\v1\PartyType
	 */
	public $Carrier;
	/**
	 * @Definition Person lodging the declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName RepresentativePerson
	 * @var fi\tulli\schema\external\ncts\dme\v1\RepresentativePersonPartyType
	 */
	public $RepresentativePerson;
	/**
	 * @Definition Principal trader party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Principal
	 */
	public $Principal;
	/**
	 * @Definition Specific circumstance code, FI Customs code list 0402.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName SpecificCircumstanceCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\SpecificCircumstanceCodeType
	 */
	public $SpecificCircumstanceCode;
	/**
	 * @Definition Transport charges payment method code, FI Customs code list 0327.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName FreightPaymentMethodCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\FreightPaymentMethodCodeType
	 */
	public $FreightPaymentMethodCode;
	/**
	 * @Definition Guarantee details.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMaxOccurs 9
	 * @xmlName Guarantee
	 */
	public $Guarantee;


} // end class DeclarationType
