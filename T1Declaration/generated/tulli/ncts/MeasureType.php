<?php

/**
 * @xmlNamespace 
 * @xmlType decimal
 * @xmlName MeasureType
 * @var MeasureType
 * @xmlDefinition Base type for a measure. A numeric value determined by measuring an object along with the unit of measure specified or implied.
 */
class MeasureType
	{


		/**
		 * @xmlType value
		 * @var float
		 */
		public $value;
	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\udt\CodeType $unitCode [optional] Unit code for the measure.
	*/                                                                        
	public function __construct($unitCode = null, $value = null)
	{
		$this->unitCode = $unitCode;
        $this->value = $value;
	}
	
	/**
	 * @Definition Unit code for the measure.
	 * @xmlType attribute
	 * @xmlName unitCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\CodeType
	 */
	public $unitCode;


} // end class MeasureType
