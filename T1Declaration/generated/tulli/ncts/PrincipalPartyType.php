<?php

/**
 * @xmlNamespace 
 * @xmlType PartyType
 * @xmlName PrincipalPartyType
 * @var PrincipalPartyType
 * @xmlDefinition Principal trader party.
 */
class PrincipalPartyType
{



	/**                                                                       
	*/                                                                        
	public function __construct($ID = null, $IDExtension = null, $Name = null, $Address = null, $NatureQualifierCode = null, $LanguageCode = null, $TIRHolderID = null)
	{
		$this->ID = $ID;
		$this->IDExtension = $IDExtension;
		$this->Name = $Name;
		$this->Address = $Address;
        $this->NatureQualifierCode = $NatureQualifierCode;
        $this->LanguageCode = $LanguageCode;
        $this->TIRHolderID = $TIRHolderID;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ID
	 * @var fi\tulli\schema\external\ncts\dme\v1\TraderIDType
	 */
	public $ID;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName IDExtension
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\IDExtensionType
	 */
	public $IDExtension;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Name
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $Name;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Address
	 */
	public $Address;

    /**
     * @Definition The basic language to be used in any further communication between the Trader and the Customs system.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMinOccurs 0
     * @xmlName LanguageCode
     * @var LanguageCodeType
     */
    public $LanguageCode;

    /**
     * @Definition Party type code, FI Customs code list 0121.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMinOccurs 0
     * @xmlName NatureQualifierCode
     * @var TraderNatureQualifierCodeType
     */
    public $NatureQualifierCode;

    /**
     * @Definition TIR holder identifier.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMinOccurs 0
     * @xmlName TIRHolderID
     * @var TIRHolderIDType
     */
    public $TIRHolderID;


} // end class PrincipalPartyType
