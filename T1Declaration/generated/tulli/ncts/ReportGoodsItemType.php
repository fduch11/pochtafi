<?php
/**
 * @xmlNamespace 
 * @xmlType DocumentType
 * @xmlName ReportGoodsItemType
 * @var ReportGoodsItemType
 */
class ReportGoodsItemType {



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType $ItemSequenceNumber [optional] Ordinal number indicating the position in a sequence, anf identifying the item within the sequence.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType $TransitTypeCode [optional] Transit declaration type code, FI Customs code list 0081.
		@param  $AdditionalDocument [optional] Additional document information.
		@param  $AdditionalInformation [optional] Additional information.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DispatchCountryCode [optional] Country of dispatch.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $DestinationCountryCode [optional] Destination country.
		@param fi\tulli\schema\external\ncts\dme\v1\TransportEquipmentType $TransportEquipment [optional] Transport equipment information.
		@param fi\tulli\schema\external\ncts\dme\v1\PackagingType $Packaging [optional] Packaging information.
		@param  $Commodity [optional] Goods commodity and classification.
		@param fi\tulli\schema\external\ncts\dme\v1\SensitiveGoodsType $SensitiveGoods [optional] Goods sensitivity information.
		@param fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType $GrossMassMeasure [optional] Total weight (mass) of goods.
		@param fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType $NetWeightMeasure [optional] Weight (mass) of the goods without packing material.
		@param  $Consignor [optional] Consignor party.
		@param  $Consignee [optional] Consignee party.
	*/                                                                        
	public function __construct($ItemSequenceNumber = null, $TransitTypeCode = null, $AdditionalDocument = null, $AdditionalInformation = null, $DispatchCountryCode = null, $DestinationCountryCode = null, $TransportEquipment = null, $Packaging = null, $Commodity = null, $SensitiveGoods = null, $GrossMassMeasure = null, $NetWeightMeasure = null, $Consignor = null, $Consignee = null)
	{
		$this->ItemSequenceNumber = $ItemSequenceNumber;
		$this->TransitTypeCode = $TransitTypeCode;
		$this->AdditionalDocument = $AdditionalDocument;
		$this->AdditionalInformation = $AdditionalInformation;
		$this->DispatchCountryCode = $DispatchCountryCode;
		$this->DestinationCountryCode = $DestinationCountryCode;
		$this->TransportEquipment = $TransportEquipment;
		$this->Packaging = $Packaging;
		$this->Commodity = $Commodity;
		$this->SensitiveGoods = $SensitiveGoods;
		$this->GrossMassMeasure = $GrossMassMeasure;
		$this->NetWeightMeasure = $NetWeightMeasure;
		$this->Consignor = $Consignor;
		$this->Consignee = $Consignee;
	}
	
	/**
	 * @Definition Ordinal number indicating the position in a sequence, anf identifying the item within the sequence.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ItemSequenceNumber
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\ItemSequenceNumberType
	 */
	public $ItemSequenceNumber;
	/**
	 * @Definition Transit declaration type code, FI Customs code list 0081.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransitTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransitTypeCodeType
	 */
	public $TransitTypeCode;
	/**
	 * @Definition Additional document information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 99
	 * @xmlName AdditionalDocument
	 */
	public $AdditionalDocument;
	/**
	 * @Definition Additional information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 99
	 * @xmlName AdditionalInformation
	 */
	public $AdditionalInformation;
	/**
	 * @Definition Country of dispatch.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DispatchCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DispatchCountryCode;
	/**
	 * @Definition Destination country.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DestinationCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $DestinationCountryCode;
	/**
	 * @Definition Transport equipment information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 99
	 * @xmlName TransportEquipment
	 * @var fi\tulli\schema\external\ncts\dme\v1\TransportEquipmentType
	 */
	public $TransportEquipment;
	/**
	 * @Definition Packaging information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMaxOccurs 99
	 * @xmlName Packaging
	 * @var fi\tulli\schema\external\ncts\dme\v1\PackagingType
	 */
	public $Packaging;
	/**
	 * @Definition Goods commodity and classification.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Commodity
	 */
	public $Commodity;
	/**
	 * @Definition Goods sensitivity information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 9
	 * @xmlName SensitiveGoods
	 * @var fi\tulli\schema\external\ncts\dme\v1\SensitiveGoodsType
	 */
	public $SensitiveGoods;
	/**
	 * @Definition Total weight (mass) of goods.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GrossMassMeasure
	 * @var fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType
	 */
	public $GrossMassMeasure;
	/**
	 * @Definition Weight (mass) of the goods without packing material.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName NetWeightMeasure
	 * @var fi\tulli\schema\external\ncts\dme\v1\WeightMeasureType
	 */
	public $NetWeightMeasure;
	/**
	 * @Definition Consignor party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignor
	 */
	public $Consignor;
	/**
	 * @Definition Consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Consignee
	 */
	public $Consignee;


} // end class ReportGoodsItemType
