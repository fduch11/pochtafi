<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName TransShipment
 * @var TransShipmentType
 * @xmlDefinition Details for transshipment en route.
 */
class TransShipmentType
{

	public function __construct($Endorsement, $NewTransportMeans, $NewTransportEquipment)
	{
        $this->Endorsement = $Endorsement;
        $this->NewTransportMeans = $NewTransportMeans;
        $this->NewTransportEquipment = $NewTransportEquipment;
	}

    /**
     * @Definition Endorsement of the reportable event that an authority has confirmed.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName Endorsement
     * @var fi\tulli\schema\external\common\dme\v1_0\ncts\EndorsementType
     */
    public $Endorsement;
	
	/**
	 * @Definition New transport means information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName NewTransportMeans
	 * @var fi\tulli\schema\external\common\dme\v1_0\ncts\TransportMeansType
	 */
	public $NewTransportMeans;

    /**
     * @Definition The new container number, written together: four letters (if they exist) + six numbers + hyphen + control number.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName NewTransportEquipment
     * @var fi\tulli\schema\external\common\dme\v1_0\ncts\TransportEquipmentType
     */
    public $NewTransportEquipment;



} // end class TransShipmentType
