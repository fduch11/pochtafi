<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName TransportMeansType
 * @var TransportMeansType
 * @xmlDefinition Transport means information.
 */
class TransportMeansType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\TransportModeCodeType $TransportModeCode [optional] Means of transport type code, FI Customs code list 0041
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $TransportMeansNationalityCode [optional] Country code of transport means registration nationality.
		@param fi\tulli\schema\external\ncts\dme\v1\TransportMeansIDType $TransportMeansID [optional] Transport means identifier.
	*/                                                                        
	public function __construct($TransportModeCode = null, $TransportMeansNationalityCode = null, $TransportMeansID = null, $ConveyanceReferenceID = null)
	{
		$this->TransportModeCode = $TransportModeCode;
		$this->TransportMeansNationalityCode = $TransportMeansNationalityCode;
		$this->TransportMeansID = $TransportMeansID;
        $this->ConveyanceReferenceID = $ConveyanceReferenceID;
	}
	
	/**
	 * @Definition Means of transport type code, FI Customs code list 0041
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransportModeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransportModeCodeType
	 */
	public $TransportModeCode;
	/**
	 * @Definition Country code of transport means registration nationality.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransportMeansNationalityCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $TransportMeansNationalityCode;
	/**
	 * @Definition Transport means identifier.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TransportMeansID
	 * @var fi\tulli\schema\external\ncts\dme\v1\TransportMeansIDType
	 */
	public $TransportMeansID;

    /**
     * @Definition Conveyance reference number to identify a journey of a means of transport, for example voyage number, flight number, trip number.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMinOccurs 0
     * @xmlName ConveyanceReferenceID
     * @var ConveyanceReferenceIDType
     */
    public $ConveyanceReferenceID;


} // end class TransportMeansType
