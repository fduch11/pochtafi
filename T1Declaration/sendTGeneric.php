<?php

set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme');
set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/ncts');
//set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme/fi/tu');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';

try {

    $local_cert = 'certificates/pem_pochta.cer';

    $soapUrl = 'https://ws-customertest.tulli.fi/services/DirectMessageExchange';
    $soapUrl = 'https://ws.tulli.fi/services/DirectMessageExchange';
//    $soapUrl = 'http://localhost:33816/CorporateMessageServiceHttpBinding.asmx';

    $soapParams = array(
//        'proxy_host' => 'localhost',
//        'proxy_port' => 8888,

        'trace' => 1,
        'exceptions' => true,
        'local_cert' => $local_cert,
        'encoding' => 'UTF-8'
    );

    $outputDir = 'C:\\Sandbox\\T1Declaration\\results\\all\\';
//    $outputDir = 'C:\\Sandbox\\T1Declaration\\results\\generic\\';

    include('TestCases/fixedCase.php');
    $fixedParams['Environment'] = 'PRODUCTION';

    $fileNames = array(
//        'TestCases/testCase01.php',
//        'TestCases/FITARR.15FI000000002204T9.php',
//        'TestCases/FITULR.15FI000000002204T9.php',
//        'TestCases/FITARR.15FI000000008982T0.php',
//        'TestCases/FITULR.15FI000000008982T0.php',
//        'TestCases/FITARR.15FI000000017406T1.php',
        'TestCases/FITULR.15FI000000017406T1.php',
//        'TestCases/testCase23.php',
//        'TestCases/testCase24.php'
    );

//    $fileNames = array(
//        'TestCases/code.dispatch/testCase1.FITARR.01.php',
//        'TestCases/code.dispatch/testCase2.FITARR.03.php',
//        'TestCases/code.dispatch/testCase3.FITARR.05.php',
//        'TestCases/code.dispatch/testCase4.FITARR.07.php',
//        'TestCases/code.dispatch/testCase5.FITARR.09.php',
//        'TestCases/code.dispatch/testCase6.FITARR.11.php',
//
//        'TestCases/code.dispatch/testCase1.FITULR.02.php',
//        'TestCases/code.dispatch/testCase2.FITULR.04.php',
//        'TestCases/code.dispatch/testCase3.FITULR.06.php',
//        'TestCases/code.dispatch/testCase4.FITULR.08.php',
//        'TestCases/code.dispatch/testCase5.FITULR.10.php',
//        'TestCases/code.dispatch/testCase6.FITULR.12.php',
//    );

//    $fileNames = glob("C:\\Sandbox\\T1Declaration/TestCases/code.dispatch/*.*");

    $controlReferences = array();
    foreach ($fileNames as $fileName) {

        include($fileName);

        try {
            $tulli = new Tulli($soapUrl, $soapParams);
            $result = $tulli->sendT1($fixedParams, $params, $text);
        }
        catch(Exception $ex) {

        }
        $baseFileName = basename($fileName, ".php");
        file_put_contents($outputDir . $baseFileName . ".request.xml", $text);

        /** @var $result UploadResponse */
        /** @var $responseHeader ResponseHeader */
        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {

            /** @var $responseMessage MessageInformation */
            $responseMessage = $result->ApplicationRequestMessageInformation;

            $controlReference = $responseMessage->ControlReference;
            $controlReferences[$baseFileName] = $controlReference;
        } else {
            var_dump($responseHeader);
        }

        sleep(2);
    }

    sleep(180);

    /* ---------  Download List -------------- */

    $requestHeader = new RequestHeader();
    $requestHeader->IntermediaryBusinessId = $fixedParams['BusinessId'];
    $requestHeader->IntermediarySoftwareInfo = $fixedParams['SoftwareInfo'];
    $requestHeader->Timestamp = time();
    $requestHeader->Language = "EN";

    $criteria = new DownloadMessageListFilteringCriteria(date('Y-m-d'), date('Y-m-d'), null, null, 'NEW');

    $downloadListRequest = new DownloadListRequest();
    $downloadListRequest->RequestHeader = $requestHeader;
    $downloadListRequest->DownloadMessageListFilteringCriteria = $criteria;

    $soapClient = new SoapClient('tulli\dme\CustomsCorporateService_soap1_2.wsdl', $soapParams);
    $soapClient->__setLocation($soapUrl);
    $result = $soapClient->__soapCall('DownloadList', array($downloadListRequest));

    $storageReferences = array();

    /** @var $result DownloadListResponse */
    $responseHeader = $result->ResponseHeader;
    if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {
        if (is_array($result->MessageInformation)) {
            foreach ($result->MessageInformation as $responseMessage) {
                $baseFileName = array_search($responseMessage->ControlReference, $controlReferences);
                if ($baseFileName) {
                    $storageReferences[$baseFileName] = $responseMessage->MessageStorageId;

                    $text = $responseMessage->ControlReference . PHP_EOL . $responseMessage->MessageStorageId;
                    file_put_contents($outputDir . $baseFileName . ".properties.txt", $text);
                }
            }
        } else if (is_object($result->MessageInformation)) {
            $baseFileName = array_search($result->MessageInformation->ControlReference, $controlReferences);
            if ($baseFileName) {
                $storageReferences[$baseFileName] = $result->MessageInformation->MessageStorageId;

                $text = $result->MessageInformation->ControlReference . PHP_EOL . $result->MessageInformation->MessageStorageId;
                file_put_contents($outputDir . $baseFileName . ".properties.txt", $text);
            }
        }
    } else {
        var_dump($responseHeader);
    }

    foreach ($storageReferences as $baseFileName => $storageReference) {

        if (!isset($storageReference)) continue;

        /* ---------  Download  -------------- */

        $criteria = new DownloadMessageFilteringCriteria($storageReference);

        $downloadRequest = new DownloadRequest();
        $requestHeader->Timestamp = time();
        $downloadRequest->RequestHeader = $requestHeader;
        $downloadRequest->DownloadMessageFilteringCriteria = $criteria;

        $result = $soapClient->__soapCall('Download', array($downloadRequest));

        /** @var $result DownloadResponse */
        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {

            $xmlObject   = new SimpleXMLElement($result->ApplicationResponseMessage);
            $content = $xmlObject->children("v1", true)->ApplicationResponseContent;
            $text = base64_decode($content->children("v11", true)->Content);

            $doc = new DOMDocument();
            $doc->loadXML($text);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            $doc->save($outputDir . $baseFileName . ".response.xml");

            $content = $xmlObject->children("v1", true)->AttachmentOfApplicationResponseContent;
            if (!empty($content)) {
                $text = base64_decode($content->children("v11", true)->Content);
                file_put_contents($outputDir . $storageReference . '.zip', $text);
            }
        } else {
            var_dump($responseHeader);
        }

        sleep(2);
    }


}
catch (SoapFault $soapFault) {
    echo $soapFault;
    var_dump($soapFault);
}
catch (Exception $ex) {
    echo $ex;
    var_dump($ex);
}

?>