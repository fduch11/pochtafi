<?xml version="1.0" encoding="UTF-8"?>
<!-- Direct Message Exchange component schema of unqualified types -->
<xs:schema xmlns:udt="http://tulli.fi/schema/external/common/dme/v1_0/udt" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://tulli.fi/schema/external/common/dme/v1_0/udt" elementFormDefault="qualified" version="v1_0">
	<xs:annotation>
		<xs:documentation xml:lang="en">
		This schema contains type definitions to be used as unqualified base for deriving type definitions for actual document constructs. Only the main characteristics of the content data shall be defined here.
		Size restrictions minLength and max length must not be set for the text types. Other vice deriving from these with fixed length facet would not be allowed according to the schema standard.
		Also a fixed length restriction should not be set here, because deriving types with length specification in the pattern facet might conflict with that.
		</xs:documentation>
	</xs:annotation>
	<!-- TEXTS -->
	<!-- Base text -->
	<xs:simpleType name="BaseTextType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for any text, and limiting accepted caharacter code group for specific language area.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[\p{IsBasicLatin}\p{IsLatin-1Supplement}]+"/>
			<!-- Pattern '\p{ls<group-name>}' specifies a character code group for a specific language area. -->
		</xs:restriction>
	</xs:simpleType>
	<!-- Descriptive text -->
	<xs:simpleType name="TextType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for any descriptive text.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:BaseTextType">
			<xs:pattern value="[\P{Cc}\s]+"/>
			<!-- Pattern '\P{Cc}' forbids character class 'Cc', which contains control characters within range 0x00..0x1F and 0x80..0x9F, i.e. any control characters (including end of line variations), except 'space' (0x20), 'non-breakin space' (0xA0) and 'soft hyphen' (0xAD). In additon, pattern '\s' for any white-spaces is added to acceptable characters. -->
		</xs:restriction>
	</xs:simpleType>
	<!-- Single line of text -->
	<xs:simpleType name="LineType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for any single line of text.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:BaseTextType">
			<xs:pattern value="[\P{Cc}\t]+"/>
			<!-- Pattern '\P{Cc}' forbids character class 'Cc', which contains control characters within range 0x00..0x1F and 0x80..0x9F, i.e. any control characters (including end of line variations), except 'space' (0x20), 'non-breakin space' (0xA0) and 'soft hyphen' (0xAD). In additon, pattern '\t' for 'horizontal tab' (0x09) is added to acceptable characters. -->
		</xs:restriction>
	</xs:simpleType>
	<!-- CODES -->
	<!-- Code -->
	<xs:simpleType name="CodeType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a code. Code name token limited to values without whitespace characters. A character string (letters, figures or symbols) that for brevity and / or language independence may be used to represent or replace a definitive value or text of a Property. [Note: The term 'Code' should not be used if the character string identifies an instance of an Object Class or an object in the real world, in which case the Representation Term identifier should be used.]</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:BaseTextType">
			<xs:pattern value="[\P{Cc}\t]+"/>
			<!-- Pattern '\P{Cc}' forbids character class 'Cc', which contains control characters within range 0x00..0x1F and 0x80..0x9F, i.e. any control characters (including end of line variations), except 'space' (0x20), 'non-breakin space' (0xA0) and 'soft hyphen' (0xAD). In additon, pattern '\t' for 'horizontal tab' (0x09) is added to acceptable characters. -->
		</xs:restriction>
	</xs:simpleType>
	<!-- IDENTIFIERS -->
	<!-- Identifier -->
	<xs:simpleType name="IdentifierType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for an identifier. Identity name token limited to values without whitespace characters. A character string to identify and distinguish uniquely, one instance of an object in an identification scheme from all other objects in the same scheme together with relevant supplementary information.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="udt:BaseTextType">
			<xs:pattern value="[\P{Cc}\t]+"/>
			<!-- Pattern '\P{Cc}' forbids character class 'Cc', which contains control characters within range 0x00..0x1F and 0x80..0x9F, i.e. any control characters (including end of line variations), except 'space' (0x20), 'non-breakin space' (0xA0) and 'soft hyphen' (0xAD). In additon, pattern '\t' for 'horizontal tab' (0x09) is added to acceptable characters. -->
		</xs:restriction>
	</xs:simpleType>
	<!-- Sequence number, usually used as an identifier and/or ordinal number within a sequence -->
	<xs:simpleType name="SequenceNumberType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a numeric ordinal identifier within a sequence.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:nonNegativeInteger"/>
	</xs:simpleType>
	<!-- CHRONOLOGY -->
	<!-- Date -->
	<xs:simpleType name="DateType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Date in format YYYY-MM-DD, without fractions of seconds or time zone information.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:date">
			<xs:pattern value="[0-9]{4}\-[0-9]{2}\-[0-9]{2}"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- Combined date and time -->
	<xs:simpleType name="DateTimeType">
		<xs:annotation>
			<xs:documentation xml:lang="en">DateTime in format YYYY-MM-DDThh:mm:ss, without fractions of seconds or time zone information.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:dateTime">
			<xs:pattern value="[0-9]{4}\-[0-9]{2}\-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}"/>
		</xs:restriction>
	</xs:simpleType>
	<!-- NUMERIC -->
	<!-- Quantity and count -->
	<xs:simpleType name="QuantityType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a quantity and a count.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:nonNegativeInteger"/>
	</xs:simpleType>
	<!-- Amount or monetary value -->
	<xs:complexType name="AmountType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for an amount and a monetary value. A number of monetary units specified in a currency where the unit of currency is explicit or implied.</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:extension base="xs:decimal">
				<xs:attribute name="currencyCode" type="udt:CodeType">
					<xs:annotation>
						<xs:documentation xml:lang="en">Currency code for the amount.</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<!-- Measure -->
	<xs:complexType name="MeasureType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a measure. A numeric value determined by measuring an object along with the unit of measure specified or implied.</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:extension base="xs:decimal">
				<xs:attribute name="unitCode" type="udt:CodeType" use="optional">
					<xs:annotation>
						<xs:documentation xml:lang="en">Unit code for the measure.</xs:documentation>
					</xs:annotation>
				</xs:attribute>
			</xs:extension>
		</xs:simpleContent>
	</xs:complexType>
	<!-- Percent ratio -->
	<xs:simpleType name="PercentType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a percent. Numeric information expressed as parts per hundred as determined by calculation.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:decimal"/>
	</xs:simpleType>
	<!-- Rate -->
	<xs:simpleType name="RateType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for a rate, etc. A stated numerical value of one thing corresponding proportionally to a certain value of some other thing.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:decimal"/>
	</xs:simpleType>
	<!-- STATE INDICATORS -->
	<!-- Indicator flag -->
	<xs:simpleType name="IndicatorType">
		<xs:annotation>
			<xs:documentation xml:lang="en">Base type for any boolean indicator flag.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:boolean"/>
	</xs:simpleType>
</xs:schema>
