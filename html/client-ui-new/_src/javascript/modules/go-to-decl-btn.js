ns('go-to-decl-btn', function() {
  var declAPI = $('.js-declaration').data('declaration-api');
  $('.js-go-to-decl-btn').click(function() {
    declAPI && declAPI.setEditMode.emit(true);
  })
});
