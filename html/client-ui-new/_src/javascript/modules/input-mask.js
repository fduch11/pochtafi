ns('input-mask', function() {

  $('input[data-mask]').each(function() {

    $(this).mask($(this).data('mask'), {placeholder: ' '});

  });

});
