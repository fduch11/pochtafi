ns('slider', function(exports) {
  $('#storage-slider').owlCarousel({

    navigation : true, // Show next and prev buttons
    navigationText : [" "," "],
    slideSpeed : 300,
    paginationSpeed : 400,
    items : 1,
    singleItem:true,
    pagination:true

    // "singleItem:true" is a shortcut for:
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

});



