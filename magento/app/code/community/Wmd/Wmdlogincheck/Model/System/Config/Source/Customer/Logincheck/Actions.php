<?php
/**
 * Wmd_Wmdlogincheck_Model_System_Config_Source_Customer_Logincheck_Actions  
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://eklatant.ch/WMD-License-Community.txt
 *
 * @category  Wmd
 * @package   Wmd_Wmdlogincheck
 * @author    Dominik Wyss <info@eklatant.ch>
 * @copyright 2011 Dominik Wyss | Digiswiss (http://www.digiswiss.ch)
 * @link      http://www.eklatant.ch/
 * @license   http://eklatant.ch/WMD-License-Community.txt
*/?>
<?php
class Wmd_Wmdlogincheck_Model_System_Config_Source_Customer_Logincheck_Actions
{
    /**
     * Actions
     * @var array
     */		
    protected $_options;
	
    /**
     * Return the avaiable customer account actions
     * 
     * @param boolean $isMultiselect if Multiselect for the order status selection is allowed 
     *
     * @return array
     */	      
	  public function toOptionArray($isMultiselect=false)
    {		
		    if (!$this->_options) { 		    
            $this->_options = array(
                array('value' => '', 'label' => Mage::helper('wmdlogincheck')->__('Disable All')),
                array('value' => '/customer_account_create/', 'label' => Mage::helper('wmdlogincheck')->__('Customer Account Create')),
                array('value' => '/customer_account_forgotpassword/', 'label' => Mage::helper('wmdlogincheck')->__('Customer Account Forgot Password')),
//                 array('value' => '/customer_account_confirmation/', 'label' => Mage::helper('wmdlogincheck')->__('Customer Account Confirmation')),
            );
            return $this->_options;
        }		
    }
}