<?php

class Iwings_Actionaudit_Block_Adminhtml_Actionaudit extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_actionaudit';
        $this->_blockGroup = 'actionaudit';
        $this->_headerText = Mage::helper('actionaudit')->__('Action Audit');
        $this->_addButtonLabel = Mage::helper('actionaudit')->__('Add Actionaudit');
        parent::__construct();
        $this->_removeButton('add');
    }
}