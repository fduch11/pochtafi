<?php
$installer = $this;
$installer->startSetup();
$installer->run("DROP TABLE IF EXISTS {$this->getTable('iwings_log_details')};
CREATE TABLE `iwings_log_details` (
  `log_id` int(11) NOT NULL auto_increment,
  `log_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `log_action` varchar(50) NOT NULL,
  `adminrole` varchar(25) NOT NULL,
  `website` varchar(25) NOT NULL,  
  `updated_by` varchar(25) NOT NULL,
  `log_entity` varchar(50) NOT NULL,
  `json_old` text NOT NULL,
  `json_new` text NOT NULL,
  PRIMARY KEY  (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
");
$installer->endSetup();
?>