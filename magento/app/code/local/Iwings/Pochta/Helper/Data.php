<?php

class Iwings_Pochta_Helper_Data extends Mage_Core_Helper_Abstract
{
    function getParcelCollectionSize($filters, $customerId) {

        $collection = $this->getParcelCollection($filters, $customerId);
        return is_array($collection) ? count($collection) : $collection->getSize();
    }

    function getParcelCollection($filters, $customerId) {

        $category = Mage::getModel('catalog/category')->loadByAttribute('name', 'User_' . $customerId);

        if ($category) {
            /** @var $productCollection Mage_Catalog_Model_Resource_Product_Collection **/
            $productCollection = $category->getProductCollection()
                ->addAttributeToFilter('client_visibility', true)
                ->addAttributeToSelect('*');

            if (isset($filters)) {
                foreach ($filters as $dummyKey => $filter) {
                    foreach ($filter as $field => $value) {
                        if (!is_array($value) || !is_array($value['nin']) || count($value['nin']) > 0) {
                            $productCollection->addAttributeToFilter($field, $value);
                        }
                    }
                }
            }
        } else {
            $productCollection = array();
        }

        return $productCollection;
    }

    public function getRefundableAmount($parcel) {
        /** @var $parcel Mage_Catalog_Model_Product */

        $result = 0.0;

        if ($parcel->getBoxOrigin() == Mage::registry('box_origin_overseas')) {

            foreach ($parcel->getUpSellProductCollection() as $serviceFee) {
                $serviceFee->load($serviceFee->getId());
                if ($serviceFee->getIsCancelable()) {
                    $result += $serviceFee->getUserPrice();
                }
            }
        }

        return $result;
    }

    public function getNonRefundableAmount($parcel) {
        /** @var $parcel Mage_Catalog_Model_Product */

        $result = 0.0;
        foreach ($parcel->getUpSellProductCollection() as $serviceFee) {
            $serviceFee->load($serviceFee->getId());
            if (!$serviceFee->getIsCancelable()) {
                $result += $serviceFee->getUserPrice();
            }
        }

        return $result;
    }

    public function isPaid($parcel) {
        return $parcel->getStatePayment() == Mage::registry('state_payment_paid');
    }

    public function getProductsByMovement($movementId) {

        return $this->getProductsByLink(Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL, $movementId);
    }

    public function getProductsByCustom($customId) {

        return $this->getProductsByLink(Inchoo_CustomLinkedProducts_Model_Catalog_Product_Link::LINK_TYPE_CUSTOM, $customId);
    }

    private function getProductsByLink($linkType, $linkId) {

        /** @var $linkedProducts Mage_Catalog_Model_Resource_Product_Link_Collection */
        $linkedProducts = Mage::getModel('catalog/product_link')->getCollection()
            ->addFieldToFilter('linked_product_id', is_array($linkId) ? array('in' => $linkId) : $linkId)
            ->addFieldToFilter('link_type_id', $linkType);

        return $linkedProducts;
    }

    public function getAttributeValue($attribute_code, $value_code)
    {
        $attributeDetails = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code);
        $value = $attributeDetails->getSource()->getOptionText($value_code);
        return $value;
    }

    public function renderPayButton($parcel ) {

        $serviceFeeAmount = Mage::helper('pochta')->getNonRefundableAmount($parcel);
        $depositFeeAmount = Mage::helper('pochta')->getRefundableAmount($parcel);

        $title = $this->__('Оплатить') . ' € ' . ($depositFeeAmount + $serviceFeeAmount);

        echo <<<EOD
        <form action="/customer/account/paymentinfo/" method="post">
            <input type="hidden" name="parcelId" value="{$parcel->getId()}" />
            <input type="submit" class="btn btn-block btn-orange" value="$title" />
        </form>
EOD;

        //$payMethod = 'paypal';
//        $payMethod = 'braintree';

//        switch ($payMethod) {
//            case 'braintree':
//                return $this->payBraintree($parcel);
//                break;
//            case 'paypal':
//                return $this->payPaypalStandardMethod($parcel);
//                break;
//        }
    }

    private function payPaypalStandardMethod($amount, $class, $title, $successUrl, $errorUrl, $customer)
    {

        $websiteID = Mage::app()->getWebsite()->getId();
        $storeID = Mage::app()->getStore()->getId();
        $sandbox_flag = Mage::app()->getWebsite($websiteID)->getConfig('payment/paypal_standard/sandbox_flag');
        $business_account = Mage::app()->getWebsite($websiteID)->getConfig('paypal/general/business_account');
        //$sale = Mage::app()->getWebsite($websiteID)->getConfig('payment/paypal_standard/payment_action');

        $sandbox = '';
        if ($sandbox_flag) {
            $sandbox = 'sandbox.';
        }

        echo <<<EOD
        <form action="https://www.{$sandbox}paypal.com/cgi-bin/webscr" method="post">

			<input type="hidden" name="cmd" value="_xclick">
			<INPUT TYPE="hidden" name="charset" value="utf-8">
			<input type="hidden" name="return" value="$successUrl">
<!--			<input type="hidden" name="notify_url" value="$successUrl"> -->
			<input type="hidden" name="cancel_return" value="$errorUrl">
			<input type="hidden" name="currency_code" value="EUR">

			<input type="hidden" name="business" value="$business_account">
			<input type="hidden" name="amount" value="$amount">
			<input type="hidden" name="no_shipping" value="1">
			<input type="hidden" name="no_note" value="1">

			<input type="hidden" name="solution_type" value="sole">
			<input type="hidden" name="landing_page" value="billing">
			<input type="hidden" name="paymentaction" value="authorization">

			<input type="hidden" name="invoice" value="blah-blah3">
			<input type="hidden" name="lc" value="RU">
			<input type="hidden" name="rm" value="2">

<!--
			<input type="hidden" name="email" value="$customer->getEmail()">
			<input type="hidden" name="first_name" value="$customer->getFirstname()">
			<input type="hidden" name="last_name" value="$customer->getLastname()">
			<input type="hidden" name="country" value="$customer->getCountry()">
			<input type="hidden" name="address_zip" value="$customer->getZip()">
			<input type="hidden" name="address_city" value="$customer->getCity()">
			<input type="hidden" name="address1" value="$customer->getDefaultBilling()">
-->
            <input type="submit" class="btn btn-block btn-orange" value="$this->__('Оплатить') . ' € ' . ($depositFeeAmount + $serviceFeeAmount)">
        </form>';
EOD;
    }

    private function payBraintree($parcel) {

        echo <<<EOD
        <form action="/customer/account/paymentform/" method="post">
            <input type="hidden" name="deposit_amount" value="<?php echo $depositFeeAmount ?>" />
            <input type="hidden" name="service_amount" value="<?php echo $serviceFeeAmount ?>" />
            <input type="hidden" name="parcelId" value="<?php echo $parcel->getId() ?>" />
            <input type="submit" class="btn btn-block btn-orange" value="<?php echo $this->__('Оплатить') . ' € ' . ($depositFeeAmount + $serviceFeeAmount)  ?>" />
        </form>
EOD;

    }

    // code copied from Mage_Catalog_Model_Product->duplicate() function
    public function getMovementData($parcel) {
        /* Prepare Cross Sell */
        $data = array();

        /** @var $parcel Mage_Catalog_Model_Product */
        if (!$parcel->isObjectNew()) {

            $parcel->getLinkInstance()->useCrossSellLinks();
            $attributes = array();
            foreach ($parcel->getLinkInstance()->getAttributes() as $_attribute) {
                if (isset($_attribute['code'])) {
                    $attributes[] = $_attribute['code'];
                }
            }
            foreach ($parcel->getCrossSellLinkCollection() as $_link) {
                $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
            }

        }

        return $data;
    }

    public function getRelatedDocsData($parcel) {
        /* Prepare Related*/
        $data = array();

        if (!$parcel->isObjectNew()) {

            $parcel->getLinkInstance()->useRelatedLinks();
            $attributes = array();
            foreach ($parcel->getLinkInstance()->getAttributes() as $_attribute) {
                if (isset($_attribute['code'])) {
                    $attributes[] = $_attribute['code'];
                }
            }
            foreach ($parcel->getRelatedLinkCollection() as $_link) {
                $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
            }

        }

        return $data;
    }

    public function getCustomData($parcel) {
        $data = array();

        if (!$parcel->isObjectNew()) {

            $parcel->getLinkInstance()->useCustomLinks();
            $attributes = array();
            foreach ($parcel->getLinkInstance()->getAttributes() as $_attribute) {
                if (isset($_attribute['code'])) {
                    $attributes[] = $_attribute['code'];
                }
            }
            foreach ($parcel->getCustomLinkCollection() as $_link) {
                $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
            }

        }

        return $data;
    }

    public function getServiceFeeData($parcel) {
        $data = array();

        if (!$parcel->isObjectNew()) {

            $parcel->getLinkInstance()->useUpSellLinks();
            $attributes = array();
            foreach ($parcel->getLinkInstance()->getAttributes() as $_attribute) {
                if (isset($_attribute['code'])) {
                    $attributes[] = $_attribute['code'];
                }
            }
            foreach ($parcel->getUpSellLinkCollection() as $_link) {
                $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
            }

        }

        return $data;
    }

    public function getCarrierName($movement) {
        $name = $movement->getCarrierNameFirst();
        if ($name && $movement->getCarrierNameLast()) {
            $name .= ' ';
        }
        $name .= $movement->getCarrierNameLast();

        return $name;
    }
}