<?php

require_once("BaseController.php");

class Iwings_Pochta_IndexController extends Iwings_Pochta_BaseController
{
    /**
     * Renders Pochta Home page
     *
     */
    public function indexAction()
    {
        if (array_key_exists('customs', $this->getRequest()->getParams())){
            $customs = $this->getRequest()->getParams()['customs'];
            if ($customs === "true") {
                Mage::getSingleton('core/session')->setCustomsEnvironment(true);
            } elseif ($customs === "false") {
                Mage::getSingleton('core/session')->setCustomsEnvironment(false);
            }
        }

//        $this->initAttributeSets();
//
//        /** @var $product Mage_Catalog_Model_Product */
//        $product = Mage::getModel('catalog/product');
//
//        $product
//            //    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID); //you can set data in store scope
//            ->setWebsiteIds(array($this->backendSiteId, $this->frontendSiteId)) //website ID the product is assigned to, as an array
//            ->setAttributeSetId($this->parcelSetId) //ID of a attribute set named 'Parcel'
//            ->setTypeId('bundle') //product type
//            ->setCreatedAt(strtotime('now')) //product creation time
//            ->setStatus(1) //product status (1 - enabled, 2 - disabled)
//
//            ->setName('ImageParcel')
//            ->setSku(uniqid('sku'))
//
//            ->setMediaGallery(array('images' => array(), 'values' => array())) //media gallery initialization
////                ->addImageToMediaGallery('media/catalog/product/pony.gif', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
//        ;
//
//        $product
//            ->addImageToMediaGallery('media/catalog/product/pony.gif', null, false, false);
//
//        $product->save();
//
////        $product->setName($product->getName() . '1');
////        $product->save();
//
////        $id = $product->getId();
////        $product = Mage::getModel('catalog/product');
////        $product->load($id);
//
//        $product->setName($product->getName() . '1');
//        $product->save();

		$this->loadLayout();
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_initLayoutMessages('customer/session');
        }
		$this->renderLayout();
    }

    /**
     * Default index action (with 404 Not Found headers)
     * Used if default page don't configure or available
     *
     */
    public function defaultIndexAction()
    {
        $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
        $this->getResponse()->setHeader('Status','404 File not found');

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render CMS 404 Not found page
     *
     * @param string $coreRoute
     */
    public function noRouteAction($coreRoute = null)
    {
        $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
        $this->getResponse()->setHeader('Status','404 File not found');

        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
        if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
            $this->_forward('defaultNoRoute');
        }
    }

    /**
     * Default no route page action
     * Used if no route page don't configure or available
     *
     */
    public function defaultNoRouteAction()
    {
        $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
        $this->getResponse()->setHeader('Status','404 File not found');

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Render Disable cookies page
     *
     */
    public function noCookiesAction()
    {
        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_COOKIES_PAGE);
        if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
            $this->_forward('defaultNoCookies');;
        }
    }

    /**
     * Default no cookies page action
     * Used if no cookies page don't configure or available
     *
     */
    public function defaultNoCookiesAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}