<?php

return;
$original_installer = $this;
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$installer->addAttributeSet('catalog_product', 'PochtaFi');
$installer->addAttributeSet('catalog_product', 'Arrival');
$installer->addAttributeSet('catalog_product', 'Departure');
$installer->addAttributeSet('catalog_product', 'Movement');
$installer->addAttributeSet('catalog_product', 'Packaging');
$installer->addAttributeSet('catalog_product', 'Parcel');
$installer->addAttributeSet('catalog_product', 'PreviousDocument');
$installer->addAttributeSet('catalog_product', 'SimpleProduct');

$productAttributes = array(
    array('AttrName' => 'carrier_name_first', 'AttrLabel' => 'Carrier Name', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Arrival'),
    array('AttrName' => 'carrier_name_last', 'AttrLabel' => 'Carrier Person', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Arrival'),
    array('AttrName' => 'carrier_car', 'AttrLabel' => 'Carrier Car', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Arrival'),
    array('AttrName' => 'carrier_date_in', 'AttrLabel' => 'Carrier In Date', 'AttrGroup' => 'General', 'AttrType' => 'datetime', 'AttrInput' => 'date', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrBackend' => 'eav/entity_attribute_backend_datetime', 'AttrFrontend' => 'eav/entity_attribute_frontend_datetime', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Arrival'),
    array('AttrName' => 'carrier_date_out', 'AttrLabel' => 'Carrier Date Out', 'AttrGroup' => 'General', 'AttrType' => 'datetime', 'AttrInput' => 'date', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrBackend' => 'eav/entity_attribute_backend_datetime', 'AttrFrontend' => 'eav/entity_attribute_frontend_datetime', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Arrival'),

    array('AttrName' => 'box_shelf', 'AttrLabel' => 'Shelf', 'AttrGroup' => 'Measure', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrVisible' => true, 'AttrSort' => true),
    array('AttrName' => 'box_origin', 'AttrLabel' => 'Origin', 'AttrGroup' => 'General', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false,
        'AttrOptions' => array('values' => array('European', 'Overseas'))),
    array('AttrName' => 'box_store', 'AttrLabel' => 'Local Store', 'AttrGroup' => 'General', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('Imatra', 'Lapa', 'Torf', 'Vantaa', 'NA'))),
    array('AttrName' => 'box_start_date', 'AttrLabel' => 'Start Date', 'AttrGroup' => 'General', 'AttrType' => 'datetime', 'AttrInput' => 'date', 'AttrRequired' => false, 'AttrApply' => 'bundle', 'AttrBackend' => 'eav/entity_attribute_backend_datetime', 'AttrFrontend' => 'eav/entity_attribute_frontend_datetime', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'box_suite_num', 'AttrLabel' => 'Suite Number', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'box_tracking_num', 'AttrLabel' => 'Tracking Number', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'box_height', 'AttrLabel' => 'Height (mm)', 'AttrGroup' => 'Measure', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrClass' => 'validate-digits', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'box_width', 'AttrLabel' => 'Width (mm)', 'AttrGroup' => 'Measure', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrClass' => 'validate-digits', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'box_length', 'AttrLabel' => 'Length (mm)', 'AttrGroup' => 'Measure', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrClass' => 'validate-digits', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),

    array('AttrName' => 'packaging_marks_id', 'AttrLabel' => 'PackagingMarksID', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Packaging'),
    array('AttrName' => 'packaging_quantity', 'AttrLabel' => 'PackageQuantity', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Packaging'),
    array('AttrName' => 'packaging_type_code', 'AttrLabel' => 'PackagingTypeCode', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrDefault' => 'CS', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'Packaging'),

    array('AttrName' => 'consignee_address_city', 'AttrLabel' => 'Consignee City', 'AttrGroup' => 'Consignee', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignee_address_country', 'AttrLabel' => 'Consignee Country', 'AttrGroup' => 'Consignee', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false,
        'AttrOptions' => array('values' => array('Russia'))),
    array('AttrName' => 'consignee_address_line', 'AttrLabel' => 'Consignee Line', 'AttrGroup' => 'Consignee', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignee_address_postcode', 'AttrLabel' => 'Consignee Postcode', 'AttrGroup' => 'Consignee', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignee_name', 'AttrLabel' => 'Consignee Name', 'AttrGroup' => 'Consignee', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),

    array('AttrName' => 'consignor_address_city', 'AttrLabel' => 'Consignor City', 'AttrGroup' => 'Consignor', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignor_address_country', 'AttrLabel' => 'Consignor Country', 'AttrGroup' => 'Consignor', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false,
        'AttrOptions' => array('values' => array('China', 'Russia', 'USA'))),
    array('AttrName' => 'consignor_address_line', 'AttrLabel' => 'Consignor Line', 'AttrGroup' => 'Consignor', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignor_address_postcode', 'AttrLabel' => 'Consignor Postcode', 'AttrGroup' => 'Consignor', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),
    array('AttrName' => 'consignor_name', 'AttrLabel' => 'Consignor Name', 'AttrGroup' => 'Consignor', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrSet' => 'Parcel', 'AttrVisible' => false, 'AttrSort' => false),

    array('AttrName' => 'prev_document_id', 'AttrLabel' => 'Prev. Document ID', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'PreviousDocument'),
    array('AttrName' => 'prev_document_typecode', 'AttrLabel' => 'Prev. Document TypeCode', 'AttrGroup' => 'General', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSet' => 'PreviousDocument'),

    array('AttrName' => 'state_control', 'AttrLabel' => 'Control Status', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrGroup' => 'Statuses', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('BlockedByCustom', 'Normal'))),
    array('AttrName' => 'state_custom', 'AttrLabel' => 'Custom Status', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrGroup' => 'Statuses', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('NotEnoughInfo', 'ReadyForT1', 'T1Accepted', 'T1Rejected', 'T1Released', 'T1Sent'))),
    array('AttrName' => 'state_logistic', 'AttrLabel' => 'Logistic', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrGroup' => 'Statuses', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('AtItella', 'AtStore1', 'AtStore2', 'AtStore3', 'Taken', 'ToStore1', 'ToStore2', 'ToStore3'))),
    array('AttrName' => 'state_payment', 'AttrLabel' => 'Payment Status', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrGroup' => 'Statuses', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('Delayed', 'DepositReturned', 'NotReady', 'Paid', 'ReadyForPayment'))),
    array('AttrName' => 'state_physical', 'AttrLabel' => 'Physical', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrGroup' => 'Statuses', 'AttrVisible' => true, 'AttrSort' => true, 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrSet' => 'Parcel',
        'AttrOptions' => array('values' => array('Broken', 'Good'))),

    array('AttrName' => 'goods_description', 'AttrLabel' => 'GoodsDescription', 'AttrGroup' => 'Commodity', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'simple', 'AttrVisible' => false, 'AttrSort' => false, 'AttrSet' => 'SimpleProduct'),
    array('AttrName' => 'tariff_classification', 'AttrLabel' => 'TariffClassification', 'AttrGroup' => 'Commodity', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true, 'AttrApply' => 'simple', 'AttrVisible' => false, 'AttrSort' => false, 'AttrSet' => 'SimpleProduct'),
    array('AttrName' => 'comment', 'AttrLabel' => 'Comment', 'AttrGroup' => 'General', 'AttrType' => 'text', 'AttrInput' => 'textarea', 'AttrRequired' => false, 'AttrApply' => '', 'AttrVisible' => true, 'AttrSort' => false),
    array('AttrName' => 'warehouse_type', 'AttrLabel' => 'Warehouse', 'AttrGroup' => 'General', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrApply' => 'bundle,virtual', 'AttrSource' => 'eav/entity_attribute_source_table', 'AttrVisible' => true, 'AttrSort' => true,
        'AttrOptions' => array('values' => array('EU', 'TT', 'VV'))),
    array('AttrName' => 'is_out', 'AttrLabel' => 'Is Out Document', 'AttrGroup' => 'General', 'AttrType' => 'int', 'AttrInput' => 'boolean', 'AttrRequired' => true, 'AttrApply' => 'virtual', 'AttrSource' => 'eav/entity_attribute_source_boolean', 'AttrVisible' => true, 'AttrSort' => true, 'AttrDefault' => '0'),
    array('AttrName' => 'date', 'AttrLabel' => 'Date', 'AttrGroup' => 'General', 'AttrType' => 'datetime', 'AttrInput' => 'date', 'AttrRequired' => true, 'AttrApply' => 'bundle', 'AttrBackend' => 'eav/entity_attribute_backend_datetime', 'AttrFrontend' => 'eav/entity_attribute_frontend_datetime', 'AttrVisible' => true, 'AttrApply' => '', 'AttrSort' => true)
);

foreach ($productAttributes as $attribute) {
    $installer->addAttribute('catalog_product', $attribute['AttrName'], array(
        'type' => $attribute['AttrType'],
        'label' => $attribute['AttrLabel'],
        'input' => $attribute['AttrInput'],
        'required' => $attribute['AttrRequired'],
        'apply_to' => $attribute['AttrApply'],
        'visible_on_front' => $attribute['AttrVisible'],
        'used_for_sort_by' => $attribute['AttrSort'],
        //'group' =>  $attribute['AttrGroup'],
        //'attribute_set' => $attribute['AttrSet'] == null ? 'PochtaFi' : $attribute['AttrSet'],
        'source' => $attribute['AttrSource'] == null ? '' : $attribute['AttrSource'],
        'backend' => $attribute['AttrBackend'] == null ? '' : $attribute['AttrBackend'],
        'frontend' => $attribute['AttrFrontend'] == null ? '' : $attribute['AttrFrontend'],
        'default' => $attribute['AttrDefault'] == null ? '' : $attribute['AttrDefault'],
        'option' => $attribute['AttrOptions'] == null ? '' : $attribute['AttrOptions'],
        'frontend_class' => $attribute['AttrClass'] == null ? '' : $attribute['AttrClass'],
        'user_defined' => true,
        'used_in_product_listing' => true,
        'is_configurable' => false,
        'is_html_allowed_on_front' => true
    ));
    $installer->addAttributeToSet('catalog_product', $attribute['AttrSet'] == null ? 'PochtaFi' : $attribute['AttrSet'], $attribute['AttrGroup'], $attribute['AttrName']);
}

//move attributes in groups
$installer->addAttributeToSet('catalog_product', 'Arrival', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'Arrival', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'Arrival', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'Arrival', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'Arrival', 'General', 'comment');

$installer->addAttributeToSet('catalog_product', 'Departure', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'Departure', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'Departure', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'Departure', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'Departure', 'General', 'carrier_car');
$installer->addAttributeToSet('catalog_product', 'Departure', 'General', 'date');
$installer->addAttributeToSet('catalog_product', 'Departure', 'General', 'comment');

$installer->addAttributeToSet('catalog_product', 'Movement', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'Movement', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'Movement', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'Movement', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'Movement', 'General', 'date');
$installer->addAttributeToSet('catalog_product', 'Movement', 'General', 'comment');
$installer->addAttributeToSet('catalog_product', 'Movement', 'General', 'box_store');
$installer->addAttributeToSet('catalog_product', 'Movement', 'General', 'warehouse_type');

$installer->addAttributeToSet('catalog_product', 'Packaging', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'Packaging', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'Packaging', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'Packaging', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'Packaging', 'General', 'comment');

$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'weight');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'image');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'small_image');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'thumbnail');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'media_gallery');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'System', 'gallery');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'General', 'warehouse_type');
$installer->addAttributeToSet('catalog_product', 'Parcel', 'General', 'comment');

$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'General', 'is_out');
$installer->addAttributeToSet('catalog_product', 'PreviousDocument', 'General', 'date');

$installer->addAttributeToSet('catalog_product', 'SimpleProduct', 'System', 'sku');
$installer->addAttributeToSet('catalog_product', 'SimpleProduct', 'System', 'name');
$installer->addAttributeToSet('catalog_product', 'SimpleProduct', 'System', 'status');
$installer->addAttributeToSet('catalog_product', 'SimpleProduct', 'System', 'visibility');
$installer->addAttributeToSet('catalog_product', 'SimpleProduct', 'System', 'price');

$installer->endSetup();
?>