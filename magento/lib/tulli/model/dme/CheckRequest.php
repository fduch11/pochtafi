<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName CheckRequest
 * @var CheckRequest
 */
class CheckRequest
	{



	/**                                                                       
		@param string $EchoRequest [optional] Insert Base64-coded EchoContent here.
	*/                                                                        
	public function __construct($RequestHeader = null, $EchoRequest = null)
	{
		$this->RequestHeader = $RequestHeader;
		$this->EchoRequest = $EchoRequest;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName RequestHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\RequestHeader
	 */
	public $RequestHeader;
	/**
	 * @Definition Insert Base64-coded EchoContent here.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName EchoRequest
	 * @var string
	 */
	public $EchoRequest;


} // end class CheckRequest
