<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName UploadRequest
 * @var UploadRequest
 * @xmlDefinition Request element for sending a application message operation.
 */
class UploadRequest
	{



	/**                                                                       
	*/                                                                        
	public function __construct($RequestHeader = null, $ApplicationRequestMessage = null)
	{
		$this->RequestHeader = $RequestHeader;
		$this->ApplicationRequestMessage = $ApplicationRequestMessage;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName RequestHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\RequestHeader
	 */
	public $RequestHeader;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ApplicationRequestMessage
	 * @var string
	 */
	public $ApplicationRequestMessage;


} // end class UploadRequest
