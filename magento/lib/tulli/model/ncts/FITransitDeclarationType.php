<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName FITransitDeclaration
 * @var FITransitDeclarationType
 * @xmlDefinition Import declaration message
 */
class FITransitDeclarationType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType $MessageInterchange [optional] Message interchange information.
		@param fi\tulli\schema\external\ncts\dme\v1\DeclarationType $Declaration [optional] Transit declaration.
		@param fi\tulli\schema\external\ncts\dme\v1\DeclarationGoodsItemType $GoodsItem [optional] Goods item information.
	*/                                                                        
	public function __construct($MessageInterchange = null, $Declaration = null, $GoodsItem = null)
	{
		$this->MessageInterchange = $MessageInterchange;
		$this->Declaration = $Declaration;
		$this->GoodsItem = $GoodsItem;
	}
	
	/**
	 * @Definition Message interchange information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageInterchange
	 * @var fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType
	 */
	public $MessageInterchange;
	/**
	 * @Definition Transit declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Declaration
	 * @var fi\tulli\schema\external\ncts\dme\v1\DeclarationType
	 */
	public $Declaration;
	/**
	 * @Definition Goods item information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMaxOccurs 999
	 * @xmlName GoodsItem
	 * @var fi\tulli\schema\external\ncts\dme\v1\DeclarationGoodsItemType
	 */
	public $GoodsItem;


} // end class FITransitDeclarationType
