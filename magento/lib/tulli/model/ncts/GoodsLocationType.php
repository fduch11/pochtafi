<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName GoodsLocationType
 * @var GoodsLocationType
 * @xmlDefinition Goods location information.
 */
class GoodsLocationType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\LocationQualifierCodeType $LocationQualifierCode [optional] Location qualifier code, FI Customs code list 0114.
		@param fi\tulli\schema\external\ncts\dme\v1\LocationIDType $LocationID [optional] Location identifier, i.e. warehouse identifier.
	*/                                                                        
	public function __construct($LocationQualifierCode = null, $LocationID = null)
	{
		$this->LocationQualifierCode = $LocationQualifierCode;
		$this->LocationID = $LocationID;
	}
	
	/**
	 * @Definition Location qualifier code, FI Customs code list 0114.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName LocationQualifierCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\LocationQualifierCodeType
	 */
	public $LocationQualifierCode;
	/**
	 * @Definition Location identifier, i.e. warehouse identifier.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName LocationID
	 * @var fi\tulli\schema\external\ncts\dme\v1\LocationIDType
	 */
	public $LocationID;


} // end class GoodsLocationType
