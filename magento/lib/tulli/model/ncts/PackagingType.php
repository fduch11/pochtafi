<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName PackagingType
 * @var PackagingType
 * @xmlDefinition Packaging information.
 */
class PackagingType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\PackagingTypeCodeType $PackagingTypeCode [optional] Package type code, FI Customs code list 0075
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\PackagingMarksIDType $PackagingMarksID [optional] Marks and numbers of packages or pieces
		@param fi\tulli\schema\external\ncts\dme\v1\PackageQuantityType $PackageQuantity [optional] Package quantity.
		@param fi\tulli\schema\external\ncts\dme\v1\PieceCountQuantityType $PieceCountQuantity [optional] Piece count.
	*/                                                                        
	public function __construct($PackagingTypeCode = null, $PackagingMarksID = null, $PackageQuantity = null, $PieceCountQuantity = null)
	{
		$this->PackagingTypeCode = $PackagingTypeCode;
		$this->PackagingMarksID = $PackagingMarksID;
		$this->PackageQuantity = $PackageQuantity;
		$this->PieceCountQuantity = $PieceCountQuantity;
	}
	
	/**
	 * @Definition Package type code, FI Customs code list 0075
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName PackagingTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\PackagingTypeCodeType
	 */
	public $PackagingTypeCode;
	/**
	 * @Definition Marks and numbers of packages or pieces
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName PackagingMarksID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\PackagingMarksIDType
	 */
	public $PackagingMarksID;
	/**
	 * @Definition Package quantity.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName PackageQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\PackageQuantityType
	 */
	public $PackageQuantity;
	/**
	 * @Definition Piece count.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName PieceCountQuantity
	 * @var fi\tulli\schema\external\ncts\dme\v1\PieceCountQuantityType
	 */
	public $PieceCountQuantity;


} // end class PackagingType
