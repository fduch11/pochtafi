<?php

/**
 * @xmlNamespace 
 * @xmlType PartyType
 * @xmlName RepresentativePersonPartyType
 * @var RepresentativePersonPartyType
 * @xmlDefinition Person lodging the declaration.
 */
class RepresentativePersonPartyType
{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\NameType $Name [optional] Name and telephone number of the representative person.
	*/                                                                        
	public function __construct($Name = null)
	{
		$this->Name = $Name;
	}
	
	/**
	 * @Definition Name and telephone number of the representative person.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Name
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $Name;


} // end class RepresentativePersonPartyType
