<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName TransportEquipmentType
 * @var TransportEquipmentType
 * @xmlDefinition Transport equipment information.
 */
class TransportEquipmentType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\TransportEquipmentIDType $TransportEquipmentID [optional] Container identifier.
	*/                                                                        
	public function __construct($TransportEquipmentID = null)
	{
		$this->TransportEquipmentID = $TransportEquipmentID;
	}
	
	/**
	 * @Definition Container identifier.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransportEquipmentID
	 * @var fi\tulli\schema\external\ncts\dme\v1\TransportEquipmentIDType
	 */
	public $TransportEquipmentID;


} // end class TransportEquipmentType
