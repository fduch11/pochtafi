<?php
return;
require_once '../../magento/app/Mage.php';
umask(0);
Mage::app(Mage::app()->getStore());
$ver = Mage::getVersion();
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
Mage::app()->cleanCache();
$enable = array();

foreach (Mage::helper('core')->getCacheTypes() as $type => $label) $enable[$type] = 1;
Mage::app()->saveUseCache($enable);
refreshCache();

function refreshCache()
{
    Mage:: getSingleton('catalog/url')->refreshRewrites();
    Mage:: getModel('catalog/product_image')->clearCache();
    Mage:: getSingleton('catalogsearch/fulltext')->rebuildIndex();
    Mage:: getSingleton('cataloginventory/stock_status')->rebuild();
    Mage:: getResourceModel('catalog/category_flat')->rebuild();
    Mage:: getResourceModel('catalog/product_flat_indexer')->rebuild();
}

echo 'Refreshing complete!';
?>