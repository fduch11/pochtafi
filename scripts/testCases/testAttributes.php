﻿<?php
return;
require_once '../../magento/app/Mage.php';
Mage::app(Mage::app()->getStore());
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);

removeUserAttributes();
runInstallScripts();
checkProductAttributes();

function runInstallScripts()
{
    require_once '../../magento/app/code/local/Iwings/Pochta/sql/pochta_setup/install-0.1.0.php';
    require_once '../../magento/app/code/local/Iwings/Pochta/sql/pochta_setup/upgrade-0.1.0-0.1.1.php';
    echo '<b>Install attributes complete!</b>' . '<br>' . '<br>';
}

function removeUserAttributes()
{
    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
    $installer->startSetup();
    $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();

    foreach ($productAttributes as $attribute) {
        if (strpbrk($attribute->getAttributeCode(), '1') != false) $installer->removeAttribute('catalog_product', $attribute->getAttributeCode());
    }
    $installer->endSetup();

    $installer = Mage::getModel('customer/entity_setup', 'core_setup');
    $installer->startSetup();
    $customerAttributes = Mage::getModel('customer/entity_attribute_collection');

    foreach ($customerAttributes as $attribute) {
        if (strpbrk($attribute->getAttributeCode(), '1') != false) $installer->removeAttribute('customer', $attribute->getAttributeCode());
    }
    $installer->endSetup();

    echo '<b>Remove attributes complete!</b>' . '<br>' . '<br>';
}

function checkProductAttributes()
{
    $productAttributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();
    $amountAttribute = 0;
    $amountAttribute1 = 0;
    $amountUserDefined = 0;

    foreach ($productAttributes as $attribute) {
        if (strpbrk($attribute->getAttributeCode(), '1') != false) $amountAttribute1++;
        if ($attribute->getIsUserDefined() == true) $amountUserDefined++;
    }

    foreach ($productAttributes as $attribute) {

        foreach ($productAttributes as $attribute1) {
            if ($attribute->getAttributeCode() . '1' == $attribute1->getAttributeCode()) {
                $attrOption = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attribute->getAttributeCode());
                $attrOption1 = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attribute1->getAttributeCode());

                $attribute->setAttributeCode($attribute1->getAttributeCode());
                $attribute->setAttributeId($attribute1->getAttributeId());

                if (($attribute->getData() == $attribute1->getData()) && ($attrOption->getSource()->getAllOptions()[0] == $attrOption1->getSource()->getAllOptions()[0])) {
                    echo $attribute->getAttributeCode() . ' ';
                    echo '<font color=green>true</font>' . '<br>';
                } else {
                    echo $attribute->getAttributeCode() . ' ';
                    echo "<font color=red>false</font>" . '<br>';
                }

                $amountAttribute++;
            }
        }
    }

    $amount = $amountUserDefined - $amountAttribute - $amountAttribute1;
    echo "<b>Conclusion (Product): compare attributes - $amountAttribute ,attributes with '1' - $amountAttribute1, user attributes - $amountUserDefined, not checked - $amount</b>";
    echo '<br>' . '<br>';
}

?>